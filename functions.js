
//FUNCIÓN PARA ACTUALIZAR EL DOM
function actualizarImagen(){
    let alt = document.getElementById("img1").alt;
    if(alt == 'img1'){
        document.getElementById("img1").src = "https://image.goat.com/750/attachments/product_template_pictures/images/059/514/704/original/793049_00.png.png";
        document.getElementById("img1").alt = 'img2';
    }else{
        document.getElementById("img1").src = "https://image.goat.com/750/attachments/product_template_pictures/images/060/411/172/original/GW0847.png.png";
        document.getElementById("img1").alt = 'img1';
    }
    
}






/**********VARIABLES***********/
const numero_5 = 5;
var cadena = "Hola mundo";
let numero_1 = 1;
let decimal = 10.2;

var personas = ["Juan", "Andrea", "Pedro", "Jose"];
//agregar elementos
personas.push("Alexis");
console.log(personas);
//eliminar último elemento
personas.pop();
console.log(personas);

/************CICLOS**************/
for(let i = 0; i < 10; i++){
    console.log(i);
}
console.log("-------------FOR----------");
for(let i = 0; i < personas.length; i++){
    console.log(personas[i]);
}
console.log("-------------FOREACH----------");
personas.forEach(element => {
    console.log(element);
});

console.log("-------------WHILE----------");
let contador = 0;
while(contador < personas.length){
    console.log(personas[contador]);

    //and => &&
    //or => ||
    if(contador == 2 ){
        console.info("Contador = 2");
    }

    ++contador;
}

//CONSOLE.LOG
console.log("Esto es un log normal");
console.info("Esto es un console info");
console.error("Esto es un console error");
console.warn("Esto es un console warning");
console.table({Llave_1: "Clave 1", Llave_2: "Clave 2"});

/**********FUNCIONES**********/
function saludar(){
    let resultado = sumar(10, 5);
    console.log(resultado);
    alert("Resultado: "+resultado);
}

function sumar(num1, num2){
    return num1+num2;
}

let funcionFlecha = ()=>{

}